﻿using System;
using System.Threading;

namespace MutexEx
{
    internal static class Program
    {
        private static readonly Mutex mut = new Mutex();
        private const int numThreads = 3;

        static void Main()
        {
            for (int i = 0; i < numThreads; i++)
            {
                Thread newThread = new Thread(UseResource)
                {
                    Name = String.Format("Thread{0}", i + 1)
                };
                newThread.Start();
            }
        }

        // shared resource
        private static void UseResource()
        {
            // Wait until it is safe to enter.
            Console.WriteLine("{0} is requesting the mutex",
                              Thread.CurrentThread.Name);
            mut.WaitOne();

            Console.WriteLine("{0} has entered the protected area",
                              Thread.CurrentThread.Name);

            Thread.Sleep(500);

            Console.WriteLine("{0} is leaving the protected area",
                Thread.CurrentThread.Name);

            // Release the Mutex.
            mut.ReleaseMutex();
            Console.WriteLine("{0} has released the mutex",
                Thread.CurrentThread.Name);
        }
    }
}
